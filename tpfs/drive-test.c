#include <stdio.h>
#include <stdlib.h>

#include "hardware.h"
#include "drive.h"

static void
empty_it()
{
    return;
}

int main(int argc, char const *argv[])
{
    unsigned int i;
    unsigned char buffer[HDA_SECTORSIZE];

    /* init hardware */
    if (init_hardware("hwconfig.ini") == 0)
    {
        fprintf(stderr, "Error in hardware initialization\n");
        exit(EXIT_FAILURE);
    }

    /* Interreupt handlers */
    for (i = 0; i < 16; i++)
        IRQVECTOR[i] = empty_it;

    /* Allows all IT */
    _mask(1);

    printf("> Lecture du disque: \n");
    printf("\t> Numero de cylindre: 0 \n");
    printf("\t> Numero de secteur: 0 \n\n");
    read_sector(buffer, 0, 0);
    dump(buffer, HDA_SECTORSIZE, 0, 1);

    printf("\n> Formattage du disque: \n");
    printf("\t> Numero de cylindre: 0 \n");
    printf("\t> Numero de secteur: 0 \n");
    printf("\t> Donnée à écrire: 0xCAFEBABE \n");
    format_sector(0, 0, 0x1, 0xCAFEBABE);
    printf(">Formatage terminé\n\n");

    printf("> Lecture du disque: \n");
    printf("\t> Numero de cylindre: 0 \n");
    printf("\t> Numero de secteur: 0 \n\n");
    read_sector(buffer, 0, 0);
    dump(buffer, HDA_SECTORSIZE, 0, 1);

    printf("\n> Ecriture du disque: \n");
    printf("\t> Numero de cylindre: 0 \n");
    printf("\t> Numero de secteur: 0 \n");
    printf("\t> Donnée à écrire: deadbeef \n");
    write_sector(0, 0, (unsigned char *)"deadbeef");
    printf("> Ecriture terminée\n\n");

    printf("> Lecture du disque: \n");
    printf("\t> Numero de cylindre: 0 \n");
    printf("\t> Numero de secteur: 0 \n\n");
    read_sector(buffer, 0, 0);
    dump(buffer, HDA_SECTORSIZE, 0, 1);

    /* and exit! */
    exit(EXIT_SUCCESS);
}
