#if !defined(DRIVE)
#define DRIVE

/* Paramètres du contrôleur IDE */
#define ENABLE_HDA 1       /* 0 => simulation du disque désactivée */
#define HDA_CMDREG 0x3F6   /* registre de commande du disque maitre */
#define HDA_DATAREGS 0x110 /* base des registres de données (r,r+1...r+15) */
#define HDA_IRQ 14         /* Interruption du disque */
#define HDA_SECTORSIZE 256
#define HDA_MAXCYLINDER 16 /*  Nombre de pistes du disque ma�tre */
#define HDA_MAXSECTOR 16   /*  Nombre de secteurs du disque ma�tre */

void dump(unsigned char *buffer,
          unsigned int buffer_size,
          int ascii_dump,
          int octal_dump);
void seek(int cylinder, int sec);
void read_sector(unsigned char *buf, int cylinder, int sector);
void read_sector_n(unsigned char *buf, int cylinder, int sector, int size);
void write_sector(unsigned int cylinder, unsigned int sector, unsigned char *buf);
void format_sector(unsigned int cylinder,
                   unsigned int sector,
                   unsigned int nsector,
                   unsigned int value);

#endif
