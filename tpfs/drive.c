#include <stdio.h>
#include <ctype.h>

#include "hardware.h"
#include "drive.h"

void dump(unsigned char *buffer,
          unsigned int buffer_size,
          int ascii_dump,
          int octal_dump)
{
    int i, j;

    for (i = 0; i < buffer_size; i += 16)
    {
        /* offset */
        printf("%.8o", i);

        /* octal dump */
        if (octal_dump)
        {
            for (j = 0; j < 8; j++)
                printf(" %.2x", buffer[i + j]);
            printf(" - ");

            for (; j < 16; j++)
                printf(" %.2x", buffer[i + j]);

            printf("\n");
        }
        /* ascii dump */
        if (ascii_dump)
        {
            printf("%8c", ' ');

            for (j = 0; j < 8; j++)
                printf(" %1c ", isprint(buffer[i + j]) ? buffer[i + j] : ' ');
            printf(" - ");

            for (; j < 16; j++)
                printf(" %1c ", isprint(buffer[i + j]) ? buffer[i + j] : ' ');

            printf("\n");
        }
    }
}

void seek(int cylinder, int sector)
{
    _out(HDA_DATAREGS, (cylinder >> 8) & 255);
    _out(HDA_DATAREGS + 1, cylinder & 255);
    _out(HDA_DATAREGS + 2, (sector >> 8) & 255);
    _out(HDA_DATAREGS + 3, sector & 255);
    _out(HDA_CMDREG, CMD_SEEK);
    _sleep(HDA_IRQ);
}

void read_sector(unsigned char *buf, int cylinder, int sector)
{
    int i;
    seek(cylinder, sector);
    _out(HDA_DATAREGS, 0);
    _out(HDA_DATAREGS + 1, 1);
    _out(HDA_CMDREG, CMD_READ);
    _sleep(HDA_IRQ);

    for (i = 0; i < HDA_SECTORSIZE; i++)
        buf[i] = MASTERBUFFER[i];
}

void read_sector_n(unsigned char *buf, int cylinder, int sector, int size)
{
    int i;
    unsigned char buffer[HDA_SECTORSIZE];
    read_sector(buffer, cylinder, sector);
    for (i = 0; i < size; i++)
        buf[i] = buffer[i];
}

void write_sector(unsigned int cylinder, unsigned int sector, unsigned char *buf)
{
    int i;
    seek(cylinder, sector);
    for (i = 0; i < HDA_SECTORSIZE; i++)
        MASTERBUFFER[i] = buf[i];

    _out(HDA_DATAREGS, 0);
    _out(HDA_DATAREGS + 1, 1);
    _out(HDA_CMDREG, CMD_WRITE);
    _sleep(HDA_IRQ);
}

void format_sector(unsigned int cylinder, unsigned int sector, unsigned int nsector, unsigned int value)
{
    seek(cylinder, sector);
    _out(HDA_DATAREGS, (nsector >> 8) & 255);
    _out(HDA_DATAREGS + 1, nsector & 255);
    _out(HDA_DATAREGS + 2, (value >> 24) & 255);
    _out(HDA_DATAREGS + 3, (value >> 16) & 255);
    _out(HDA_DATAREGS + 4, (value >> 8) & 255);
    _out(HDA_DATAREGS + 5, value & 255);
    _out(HDA_CMDREG, CMD_FORMAT);
    _sleep(HDA_IRQ);
}