#include <stdio.h>
#include <stdlib.h>

#include "hardware.h"
#include "vol.h"

static void
empty_it()
{
    return;
}

int main(int argc, char const *argv[])
{
    unsigned int i;

    /* init hardware */
    if (init_hardware("hwconfig.ini") == 0)
    {
        fprintf(stderr, "Error in hardware initialization\n");
        exit(EXIT_FAILURE);
    }

    /* Interreupt handlers */
    for (i = 0; i < 16; i++)
        IRQVECTOR[i] = empty_it;

    /* Allows all IT */
    _mask(1);

    load_mbr();
    display_vols();

    /* and exit! */
    exit(EXIT_SUCCESS);
}
