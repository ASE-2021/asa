#include <stdio.h>
#include <stdlib.h>

#include "hardware.h"
#include "drive.h"

static void
empty_it()
{
    return;
}

int main(int argc, char const *argv[])
{
    unsigned int i;
    unsigned char buffer[HDA_SECTORSIZE];

    if (argc != 3)
    {
        fprintf(stderr, "Usage: %s <cylinder> <sector>\n", argv[0]);
        exit(EXIT_FAILURE);
    }

    /* init hardware */
    if (init_hardware("hwconfig.ini") == 0)
    {
        fprintf(stderr, "Error in hardware initialization\n");
        exit(EXIT_FAILURE);
    }

    /* Interreupt handlers */
    for (i = 0; i < 16; i++)
        IRQVECTOR[i] = empty_it;

    /* Allows all IT */
    _mask(1);

    read_sector(buffer, atoi(argv[1]), atoi(argv[2]));
    dump(buffer, HDA_SECTORSIZE, 0, 1);

    /* and exit! */
    exit(EXIT_SUCCESS);
}
