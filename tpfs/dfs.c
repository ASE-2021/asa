#include <stdio.h>
#include <stdlib.h>

#include "hardware.h"
#include "vol.h"

static void
empty_it()
{
    return;
}

int main(int argc, char const *argv[])
{
    unsigned int i;

    /* init hardware */
    if (init_hardware("hwconfig.ini") == 0)
    {
        fprintf(stderr, "Error in hardware initialization\n");
        exit(EXIT_FAILURE);
    }

    /* Interreupt handlers */
    for (i = 0; i < 16; i++)
        IRQVECTOR[i] = empty_it;

    /* Allows all IT */
    _mask(1);

    if (argc != 2)
    {
        fprintf(stderr, "Usage: %s <volume>\n", argv[0]);
        exit(EXIT_FAILURE);
    }

    load_mbr();
    load_super(atoi(argv[1]));

    printf("--------------------------\n");
    printf("Id: %d\n", super.id);
    printf("Nom: %s\n", super.name);
    printf("N° serie: %d\n", super.serie);
    printf("Prochain bloc: %d\n", super.first_free);
    printf("Blocs libres: %d\n", super.nb_free_blocs);
    printf("--------------------------\n");

    new_bloc();
    new_bloc();
    new_bloc();
    new_bloc();

    free_bloc(2);
    free_bloc(1);

    printf("%d\n", new_bloc());
    printf("Blocs libres: %d\n", super.nb_free_blocs);
    printf("Prochain bloc: %d\n", super.first_free);

    /* and exit! */
    exit(EXIT_SUCCESS);
}
