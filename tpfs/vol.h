#if !defined(VOL_H)
#define VOL_H

#define VOLS_MAX 8
#define MAGIC 0xCAFE /* Savoir si le mbr a déjà été initialisé ou non */
#define FMT_DATA 0xCAFEBABE
#define FMT_SIZE 0x1
#define SUPER_BLOC 0

typedef enum
{
    BASE,
    ANNEXE,
    OTHER
} vtypes_t; /* Volume type */

typedef struct vol_s
{
    unsigned int sector;
    unsigned int cylinder;
    unsigned int nb_blocs;
    vtypes_t type;
} vol_t;

typedef struct mbr_s
{
    unsigned int magic;
    unsigned int nb_vols; /* nombre de volumes actuels */
    vol_t vols[VOLS_MAX];
} mbr_t;

mbr_t mbr;

typedef struct vsuper_s
{
    unsigned int magic;
    unsigned int serie;
    char name[32];
    unsigned int id;
    unsigned int first_free;
    unsigned int nb_free_blocs;
} vsuper_t;

vsuper_t super;

typedef struct vblockchain_s
{
    unsigned int next;
} vblockchain_t;

void load_mbr(void);
void save_mbr(void);
void get_sector_cylinder(unsigned int vol, unsigned int nbloc, unsigned int *sector_cylinder);

void read_bloc(unsigned int vol, unsigned int nbloc, unsigned char *buffer);
void read_bloc_n(unsigned int vol, unsigned int nbloc, unsigned char *buffer, unsigned int size); /*stack smashing error prevention*/
void write_bloc(unsigned int vol, unsigned int nbloc, unsigned char *buffer);
void format_vol(unsigned int vol);

void create_vol(unsigned int nb_blocs, unsigned int cylinder, unsigned int sector);
void display_vols();
void remove_vol(int vol);

void init_volume(unsigned int vol, char *name);
int load_super(unsigned int vol);
void save_super();
unsigned int new_bloc();
void free_bloc(unsigned int bloc);

#endif
