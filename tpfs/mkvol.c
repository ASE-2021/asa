#include <stdio.h>
#include <stdlib.h>

#include "hardware.h"
#include "vol.h"

static void
empty_it()
{
    return;
}

int main(int argc, char const *argv[])
{
    unsigned int i;

    /* init hardware */
    if (init_hardware("hwconfig.ini") == 0)
    {
        fprintf(stderr, "Error in hardware initialization\n");
        exit(EXIT_FAILURE);
    }

    /* Interreupt handlers */
    for (i = 0; i < 16; i++)
        IRQVECTOR[i] = empty_it;

    /* Allows all IT */
    _mask(1);

    if (argc != 4)
    {
        fprintf(stderr, "Usage: %s <number of blocs> <cylinder> <sector> \n", argv[0]);
        exit(EXIT_FAILURE);
    }

    load_mbr();
    create_vol(atoi(argv[1]), atoi(argv[2]), atoi(argv[3]));
    save_mbr();

    /* and exit! */
    exit(EXIT_SUCCESS);
}
