#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "hardware.h"
#include "drive.h"

#define FMT_DATA 0xCAFEBABE
#define FMT_SIZE 0x1

static void
empty_it()
{
    return;
}

int main(int argc, char const *argv[])
{
    unsigned int i;
    int cylinder, sector;

    /* init hardware */
    if (init_hardware("hwconfig.ini") == 0)
    {
        fprintf(stderr, "Error in hardware initialization\n");
        exit(EXIT_FAILURE);
    }

    /* Interreupt handlers */
    for (i = 0; i < 16; i++)
        IRQVECTOR[i] = empty_it;

    /* Allows all IT */
    _mask(1);

    if (argc == 1)
    {
        for (cylinder = 0; cylinder < HDA_MAXCYLINDER; cylinder++)
        {
            for (sector = 0; sector < HDA_MAXSECTOR; sector++)
                format_sector(cylinder, sector, FMT_SIZE, FMT_DATA);
        }
    }
    else if (argc == 2 && (!strcmp(argv[1], "-r")))
    {
        for (cylinder = HDA_MAXCYLINDER - 1; cylinder > -1; cylinder--)
        {
            for (sector = HDA_MAXSECTOR - 1; sector > -1; sector--)
                format_sector(cylinder, sector, FMT_SIZE, FMT_DATA);
        }
    }
    else if (argc == 3)
    {
        format_sector(atoi(argv[1]), atoi(argv[2]), FMT_SIZE, FMT_DATA);
    }
    else
    {
        fprintf(stderr, "Usage: %s <cylinder> <sector>\n", argv[0]);
        exit(EXIT_FAILURE);
    }

    /* and exit! */
    exit(EXIT_SUCCESS);
}
