#include <stdio.h>
#include <stdlib.h>

#include "hardware.h"
#include "vol.h"

static void
empty_it()
{
    return;
}

int main(int argc, char const *argv[])
{
    unsigned int i;

    /* init hardware */
    if (init_hardware("hwconfig.ini") == 0)
    {
        fprintf(stderr, "Error in hardware initialization\n");
        exit(EXIT_FAILURE);
    }

    /* Interreupt handlers */
    for (i = 0; i < 16; i++)
        IRQVECTOR[i] = empty_it;

    /* Allows all IT */
    _mask(1);

    load_mbr();

    printf("> Lister les volumes: \n");
    display_vols();

    printf("\n> Création d'un volume\n");
    printf("\t> Nombre de blocs: 10 \n");
    printf("\t> Numero de cylindre: 0 \n");
    printf("\t> Numero de secteur: 0 \n");
    create_vol(10, 0, 0);

    printf("\n> Création d'un volume\n");
    printf("\t> Nombre de blocs: 10 \n");
    printf("\t> Numero de cylindre: 0 \n");
    printf("\t> Numero de secteur: 1 \n");
    create_vol(10, 0, 1);
    printf("> Volume créé \n\n");

    printf("\n> Création d'un volume\n");
    printf("\t> Nombre de blocs: 10 \n");
    printf("\t> Numero de cylindre: 0 \n");
    printf("\t> Numero de secteur: 2 \n");
    create_vol(10, 0, 2);

    printf("> Lister les volumes: \n");
    display_vols();

    printf("\n> Suppression de volume\n");
    printf("\t> Numero de volume: 1 \n");
    remove_vol(1);

    printf("\n> Suppression de volume\n");
    printf("\t> Numero de volume: 0 \n");
    remove_vol(0);
    printf("> Volume supprimé \n\n");

    printf("> Lister les volumes: \n");
    display_vols();

    printf("\n> Création d'un volume après qu'on ait déjà créé 8 volumes\n");
    for (i = 0; i < VOLS_MAX + 1; i++)
    {
        create_vol(1, 0, i + 1);
    }

    save_mbr();

    /* and exit! */
    exit(EXIT_SUCCESS);
}
