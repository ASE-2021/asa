# TPs ASA

**Auteur:** Thierno-Souleymane BAH

## Système de fichiers

### TP1 : Programmation d'un controleur de disque pour lire et formater

Les programmes permettant d'afficher le contenu d'un secteur, de supprimer un secteur et d'écrire dans sur un secteur ont été implémentés. Aussi, nous avons organisé les fonctions utiles en librairies.

Tout le travail du TP1 est accessible sur le tag git _tp1_, les instructions suivantes vous permettrons de tester notre travail:

- Se mettre dans le dossier tpfs et compiler le projet

```bash
    $ cd tpfs && make
```

- Exécuter display_sector : Afficher le contenu d'un secteur

```bash
    $ ./display_sector <cylinder> <sector>
```

- Exécuter write_sector : Écrire sur un secteur

```bash
    $ ./write_sector <cylinder> <sector> <data>
```

- Exécuter format_sector : Formatter un secteur ou tous les secteurs

  - Formatter tous les secteurs

  ```bash
  $ ./format_sector
  ```

  - Formatter un secteur spécfique

  ```bash
  $ ./format_sector <cylinder> <sector>
  ```

### TP2 : partitionnement d'un disque

Dans cette partie, il s'agissait de créer des programmes de création/suppression/listage de partitions et une bibliothèque de lecture/ecriture/formattage de partitions.

Vous pouvez tester notre travail en suivant les instructions suivantes:

- Se mettre dans le dossier tpfs et compiler le projet

```bash
    $ cd tpfs && make
```

- Exécuter mkvol : Créer un volume

```bash
    $ ./mkvol <number of blocs> <cylinder> <sector>
```

- Exécuter dvol : Lister tous les volumes

```bash
    $ ./dvol
```

- Exécuter rmvol : Supprimer un volume

```bash
    $ ./rmvol
```
